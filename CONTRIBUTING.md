# Contribution Guidelines

Thank you for considering contributing to our resource repository! Your contributions help build a valuable resource for students. Follow these guidelines to contribute:

## How to Contribute

1. **Create a GitLab Account:**
   - If you don't have a GitLab account, create one [here](https://gitlab.com/users/sign_in).

2. **Access the Repository:**
   - Open the [main repository](https://gitlab.com/coder-zs-cse/NIT-Goa-Resources).

3. **Launch GitLab IDE:**
   - Press the `.` key on your keyboard to launch GitLab IDE in your browser.

4. **Upload Resources:**
   - Navigate to the appropriate directory for your resources.
   - Upload your PDF files or other resources.
   - If the directory is missing, create it. Refer to existing directories for guidance.

5. **Submit a Pull Request:**
   - After uploading, submit a pull request.

## Directory Structure

- Follow the existing directory structure for consistency.
- If unsure, check how other directories are maintained.

## Repository owner

If you have any questions or doubts, feel free to contact the current owner of this repository:

- **Zubin Shah**
  - Email: [wcoderzs@gmail.com](mailto:wcoderzs@gmail.com)
  - Linkedin Profile: [Zubin Shah](https://linkedin.com/in/zubinshah1/)


## Thank You

Your contribution is highly appreciated! 🙌 Your name will be added to the Readme as a sign of gratitude.

Happy contributing! 😊
